# drunk_player

## Description

oui non oui 

-item 1
-item 2

## Dépendances 

- openCV
- boost

## Utilisation 

```bash
./drunk_player_gui.out ../data
```

![](drunk_player_gui.png)

[dépôt gitlab](https://gitlab.com/huyghes-antoine/cicd)
lien explicite: <https://gitlab.com/huyghes-antoine/cicd>

code inline `int main() {return 0;}`

*italique*
**gras**

> bloc 
> de code

- item
- item:
	- sous-item
	- sous-item
